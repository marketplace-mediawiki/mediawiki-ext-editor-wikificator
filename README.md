# Information / Информация

Интеграция Викификатора в редактор.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_EditorWikificator`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_EditorWikificator' );
```

## Syntax / Синтаксис

```html

```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
